#%% Fourier Series Approximation
import numpy as np
import matplotlib.pyplot as plt
#%% 
dx = 0.01
L = 10 # np.pi
x = np.arange(dx, L+dx, dx)
n = len(x)
nquart = int(np.floor(n/4))
f = np.zeros_like(x)
f[nquart:2*nquart] = 4/n*np.arange(1, nquart+1)
f[2*nquart:3*nquart] = 1 - 4/n*np.arange(1, nquart+1)

plt.plot(x, f)
plt.show()
#%%
def fourier_series(f, n):
    s = np.zeros(n*2+1, dtype=np.complex128)
    L = len(f)
    t = np.arange(0, L, 1)
    f1 = 2j*np.pi/L
    for k in range(-n, n+1):
        ek = np.exp(-f1*k*t)
        s[k+n] = (f*ek).sum() / L
        print(s[k+n])
    return s
#%%
a = fourier_series(f, 5)
print(a)
#%%
x = np.arange(0, len(f)*2, 1)
f_hat = 0
f1 = 2j*np.pi/len(f)
for k in range(-n, n+1):
    f_hat += a[k+n] * np.exp(f1*k*x)
plt.plot(f_hat.real)
plt.plot(f)
plt.show()

#%%
x_hat = 0
n = 5
L = len(f)
f1 = 2j*np.pi/L
x = np.arange(0, L, 1)
for k in range(-n, n+1):
    c = (f*np.exp(-f1*k*x)).sum() / len(x)
    print(c)
    x_hat += c*np.exp(f1*k*x)
plt.plot(x, f)
plt.plot(x, x_hat.real)
plt.show()

#%%
s = np.array([
    (100, 100),
    (150, 100),
    (150, 150),
    (100, 150),
    (100, 100)
])
#%%
xp = [0, 1, 2, 3, 4]
x = np.arange(0, 4, 0.01)
sp0 = np.interp(x, xp, s[:, 0])
sp1 = np.interp(x, xp, s[:, 1])
sp = sp0 + 1j*sp1
#%%

# %%
result = fourier_series(sp, 10)
np.save('result.npy', result)

#%%
n = 10
t= np.arange(0, 4, 0.01)
f1 = 2j*np.pi/len(sp)
for ti in t:
    f_hat = 0
    for k in range(-n, n+1):
        f_hat += result[k+n] * np.exp(f1*k*ti)
    print(f_hat)
# %%
