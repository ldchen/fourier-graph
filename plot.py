#%%
import numpy as np
import cv2 as cv
import pygame
img = cv.imread('a.jpg')
scale = 0.45
n = 128
img = cv.resize(img, (int(img.shape[1]*scale), int(img.shape[0]*scale)))
dim = img.shape[1], img.shape[0]
running = True
star = np.load('star.npy')
canvas = np.load('canvas.npy')
frods = np.load('frods.npy')
rod_color = np.load('rcolor.npy')
dots = np.load('dots.npy')
L = frods.shape[0]
# print(frods.shape)
idx = 0
screen = pygame.display.set_mode(dim)
pygame.init()
while running:
    pygame.time.delay(10)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:    
            running = False
    screen.fill((0, 0, 0))
    for _ in range(16):
        px, py = dots[idx]
        ppx = int(px) if px < dim[0] else dim[0]-4
        ppy = int(py) if py < dim[1] else dim[1]-4
        d = 2
        canvas[ppx:ppx+d, ppy:ppy+d] = star[ppx:ppx+d, ppy:ppy+d, ::-1]
        idx = 0 if idx==(L-1) else idx +1
        # idx = idx +1 if idx<L-1 else 0
    surf = pygame.surfarray.make_surface(canvas)
    screen.blit(surf, (0, 0))
    for k in range(2*n+1):
        # if k == 2*n:
        color = rod_color[k] 
        pygame.draw.circle(screen, (128, 64, n//2+k//4), frods[idx, k, :2], frods[idx, k, 4], 1)
        pygame.draw.line(screen, color, frods[idx, k, :2], frods[idx, k, 2:4], 1)
    pygame.display.flip()

pygame.quit()
# %%
