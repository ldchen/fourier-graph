import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
img = cv.imread('a.jpg')
scale = 0.45
img = cv.resize(img, (int(img.shape[1]*scale), int(img.shape[0]*scale)))
dim = img.shape[1], img.shape[0]
draw = False
n = 128
time_div = 1 # Ploting Density
# draw =True
#%%
if draw:
    # img = cv.imread('jiao.png')
    # img = np.zeros((dim[1], dim[0]), dtype=np.uint8)
    pts0 = []
    img_show = img.copy()
    def hover(event, x, y, flags, param):
            # if len(pts)>1:
                # cv.line(img_show, pts[i], pts[i+1], (0, 255, 255), 1)
        # if event == cv.EVENT_MOUSEMOVE:
            # if len(pts)>0:
                # cv.line(img_show, pts[-1], (x, y), (128, 255, 255), 1)
        if flags == cv.EVENT_FLAG_LBUTTON:
            pts0.append((x, y))
            img_show[y, x] = [255, 255, 0]
            cv.circle(img_show, pts0[i], 2, (255, 250, 0), -1)
        # for i in range(len(pts0)-1):
            # cv.circle(img_show, pts0[i], 2, (255, 250, 0), -1)
        cv.imshow('test', img_show)
    cv.imshow('test', img)
    cv.setMouseCallback('test', hover)
    cv.waitKey(0)
    pts = [(x-dim[0]//2, y-dim[1]//2) for x, y in pts0] 
    s = np.array(pts)
    np.save('pts.npy', s)


s = np.load('pts.npy')
s = s[::5]
xp = list(range(s.shape[0]))
x = np.arange(0, len(xp), 1)
sp0 = np.interp(x, xp, s[:, 0])
sp1 = np.interp(x, xp, s[:, 1])
sp = sp0 + 1j*sp1

if n > len(x):
    n = len(x) -1

kidx = [0]
for _ in range(1, n+1):
    kidx.append(_)
    kidx.append(-_)

def fourier_series(f, n):
    s = np.zeros(n*2+1, dtype=np.complex128)
    L = len(f)
    t = np.arange(0, L, 1)
    f1 = 2j*np.pi/L
    for k in kidx:
        ek = np.exp(-f1*k*t)
        s[k+n] = (f*ek).sum() / L
    return s

f_term = fourier_series(sp, n)
star = cv.imread('star.png')
star = np.tile(star, (4, 4, 1))
star = star[:dim[0], :dim[1]]

#%%
# Period L = len(sp)

pts = []
dotdone = False
L = len(sp)
f1 = 2j*np.pi/L
ccnt = 0
canvas = np.zeros((dim[0], dim[1], 3), np.uint8)
# canvas = np.swapaxes(img[:,:,::-1], 0, 1)
# canvas = img*0.5
tidx = np.arange(0, L, time_div)
L = len(tidx)
ek = np.zeros((len(kidx), len(tidx)), dtype=np.complex128)
for k in kidx:
    ek[k+n] = np.exp(f1*k*tidx)
ktmp = [(k+n+10)/(2*n+1) for k in kidx]
rod_color = (plt.get_cmap('turbo')(ktmp)[:,:3] * 255).astype(np.uint8)
## Draw dots
idx = 0
dots = np.zeros((L, 2))
frods = np.zeros((L, len(kidx), 5))
# while idx < L:
for idx in range(L):
    print(idx, L)
    prev = (dim[0]//2, dim[1]//2)
    for k, fk in zip(kidx, f_term):
        f_hat = f_term[k+n] * ek[k+n][idx]
        px, py = prev[0] + f_hat.real, prev[1] + f_hat.imag
        frods[idx, k+n, 0] = prev[0]
        frods[idx, k+n, 1] = prev[1]
        frods[idx, k+n, 2] = px
        frods[idx, k+n, 3] = py
        frods[idx, k+n, 4] = abs(f_hat)
        # frod.append((prev, (px, py), abs(f_hat)))
        prev = (px, py)
    ppx = int(px) if px < dim[0] else dim[0]-1
    ppy = int(py) if py < dim[1] else dim[1]-1
    dots[idx, 0] = ppx
    dots[idx, 1] = ppy
    canvas[ppx, ppy] = [128, 128, 128]
    # idx += 1
# frods = np.array(frods, dtype=object)
np.save('star.npy', star)
np.save('canvas.npy', canvas)
np.save('frods.npy', frods)
np.save('rcolor.npy', rod_color)
np.save('dots.npy', dots)
# np.save('dots.npy', dots)